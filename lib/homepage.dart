import 'dart:convert';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:http/http.dart' as http;

import 'bo/message.dart';

class HomePage extends StatefulWidget {
  const HomePage({Key? key}) : super(key: key);

  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  List<Message> listeMessages = [];

  TextEditingController tecMSG = new TextEditingController();

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    _getMessage();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: Text("Demo liste")),
      body: Column(
        children: [
          Expanded(
            child: ListView.separated(
                separatorBuilder: (context, index) => const Divider(),
                itemCount: listeMessages.length,
                itemBuilder: (context, index) {
                  return ListTile(
                    title: Row(
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: [
                        Text(listeMessages[index].author.username),
                        Spacer(),
                        Text(listeMessages[index].created_at.minute.toString(),
                            style: TextStyle(
                              fontSize: 25.0,
                            )),
                      ],
                    ),
                    subtitle: Text(listeMessages[index].content ?? ''),
                  );
                }),
          ),
          _buildRowFields(),
        ],
      ),
    );
  }

  Widget _buildRowFields() {
    return Container(
      decoration: BoxDecoration(color: Theme.of(context).primaryColor),
      child: Row(
        children: [
          Flexible(
            child: Padding(
              padding: const EdgeInsets.all(8.0),
              child: TextField(
                controller: tecMSG,
                textInputAction: TextInputAction.done,
                onSubmitted: _sendMessage,
                decoration: const InputDecoration(
                  label: Text('Tapez votre message'),
                  labelStyle: TextStyle(color: Colors.white),
                ),
              ),
            ),
          ),
          ElevatedButton(
              onPressed: () async {
                await _sendMessage(tecMSG.text);
              },
              child: Icon(Icons.send)),
        ],
      ),
    );
  }

  Future<void> _sendMessage(String messageContent) async {
    var storage = FlutterSecureStorage();
    String? jwt = await storage.read(key: "jwt");
    print(jwt);
    if (jwt != null) {
      http.Response responseMessages = await http
          .post(Uri.parse('https://flutter-learning.mooo.com/messages'), body: {
        "content": tecMSG.text,
      }, headers: <String, String>{
        "Authorization": "Bearer " + jwt
      });
      if (responseMessages.statusCode == 200) {
        print(responseMessages.body);

        Message newMessage =
            Message.fromJson(jsonDecode(responseMessages.body));
        _onAddMessage(newMessage);
      }
    }
  }

  _getMessage() async {
    var storage = FlutterSecureStorage();
    String? jwt = await storage.read(key: "jwt");
    print(jwt);
    if (jwt != null) {
      http.Response responseMessages = await http.get(
          Uri.parse('https://flutter-learning.mooo.com/messages'),
          headers: <String, String>{"Authorization": "Bearer " + jwt});
      if (responseMessages.statusCode == 200) {
        print(responseMessages.body);

        var mapMessages = jsonDecode(responseMessages.body);
        List<Message> messages = List<Message>.from(
            mapMessages.map((messages) => Message.fromJson(messages)));
        _onReloadListView(messages);
      }
    }
  }

  _onReloadListView(List<Message> msg) {
    setState(() {
      listeMessages = msg;
      tecMSG.clear();
    });
  }

  _onAddMessage(Message msg) {
    setState(() {
      listeMessages.add(msg);
      tecMSG.clear();
    });
  }
}
