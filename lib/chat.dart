import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;

import 'bo/message.dart';

class Chat extends StatefulWidget {
  static const String id = 'Chat';
  Chat({Key? key}) : super(key: key);
  @override
  _ChatState createState() => _ChatState();
}

class _ChatState extends State<Chat> {
  List<String> listeMessages = <String>[
    "hello",
    "what's up ",
    "hi",
    "ceci est un message très long",
    "blah blah",
    "lorem ipsum",
    "hello",
    "what's up ",
    "hi",
    "ceci est un message très long",
    "blah blah",
    "lorem ipsum",
    "hello",
    "what's up ",
    "hi",
    "ceci est un message très long",
    "blah blah",
    "lorem ipsum",
    "hello",
    "what's up ",
    "hi",
    "ceci est un message très long",
    "blah blah",
    "lorem ipsum" "hello",
    "what's up ",
    "hi",
    "ceci est un message très long",
    "blah blah",
    "lorem ipsum" "hello",
    "what's up ",
    "hi",
    "ceci est un message très long",
    "blah blah",
    "lorem ipsum"
  ];
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    _getMessage();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: Text("Demo liste")),
      body: Column(
        children: [_buildListView(), buildRowInput()],
      ),
    );
  }

  Container buildRowInput() {
    return Container(
      decoration: BoxDecoration(color: Theme.of(context).primaryColor),
      child: Row(
        children: [
          Flexible(
              child: Padding(
            padding: const EdgeInsets.all(8.0),
            child: TextField(
              style: TextStyle(color: Colors.white),
              decoration: InputDecoration(
                labelText: "Tappez un nouveau message",
                labelStyle: TextStyle(color: Colors.grey),
              ),
            ),
          )),
          TextButton(
              onPressed: () {},
              child: Icon(
                Icons.send,
                color: Colors.white,
              ))
        ],
      ),
    );
  }

  Expanded _buildListView() {
    return Expanded(
      child: ListView.builder(
        itemCount: listeMessages.length,
        itemBuilder: (context, index) {
          if (index.isOdd) return Divider();
          return ListTile(
            title: Row(
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                Text("nom d\'utilisateur "),
                Spacer(),
                Text("Date / heure",
                    style: TextStyle(
                      fontSize: 10.0,
                    )),
              ],
            ),
            subtitle: Text(
              "contenu message",
            ),
          );
        },
      ),
    );
  }

  _getMessage() async {
    var responseMessages = await http.get(
        Uri.parse("https://flutter-learning.mooo.com/messages"),
        headers: <String, String>{
          "Authorization": "Bearer" +
              "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6MSwiaWF0IjoxNjM0MDQ2MDI1LCJleHAiOjE2MzY2MzgwMjV9.L6frzlnrzsGr0OgRxI76Ie2pv5o85lvZZ0ARtBNJN4M"
        });
    if (responseMessages.statusCode == 200) {
      print(responseMessages.body);
      var aapMessages = jsonDecode(responseMessages.body);
      List<Message> ListMessages = List<Message>.from(
          aapMessages.map((messages) => Message.fromJson(messages)));
      ListMessages.forEach((element) {
        print(element.content);
      });
    }
  }
}
