import 'dart:convert';
import 'dart:io';

import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:http/http.dart' as http;

class LoginPage extends StatefulWidget {
  const LoginPage({Key? key}) : super(key: key);

  @override
  _LoginPageState createState() => _LoginPageState();
}

class _LoginPageState extends State<LoginPage> {
  TextEditingController tecIdentifier = TextEditingController();
  TextEditingController tecPassword = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: const Text("Enregistrement")),
      body: Padding(
        padding: const EdgeInsets.symmetric(horizontal: 100.0),
        child: _buildColumnFields(),
      ),
    );
  }

  Widget _buildColumnFields() {
    return Column(
      mainAxisSize: MainAxisSize.min,
      children: [
        const Spacer(),
        TextField(
          controller: tecIdentifier,
          textInputAction: TextInputAction.next,
          keyboardType: TextInputType.emailAddress,
          decoration: const InputDecoration(
              label: Text('Nom d\'e-mail'),
              prefixIcon: Icon(Icons.perm_identity)),
        ),
        TextField(
          controller: tecPassword,
          textInputAction: TextInputAction.done,
          obscureText: true,
          decoration: const InputDecoration(
              label: Text('Mot de passe'), prefixIcon: Icon(Icons.password)),
        ),
        const Spacer(),
        Padding(
          padding: const EdgeInsets.all(8.0),
          child: ElevatedButton(
              onPressed: _onLogin, child: const Text('SE CONNECTER')),
        ),
        const Spacer(),
        Padding(
          padding: const EdgeInsets.all(8.0),
          child: ElevatedButton(
              onPressed: _onRegister,
              child: const Text('VOUS N\'AVEZ PAS DE COMPTE ? S\'ENREGISTRER')),
        ),
        const Spacer()
      ],
    );
  }

  _onLogin() async {
    print('email : ${tecIdentifier.text}');
    print('password : ${tecPassword.text}');
    String identifier = tecIdentifier.text;
    String password = tecPassword.text;

    //flutter pub add http

    var responseRegister = await http
        .post(Uri.parse('https://flutter-learning.mooo.com/auth/local'), body: {
      "identifier": identifier,
      "password": password,
    });
    try {
      if (responseRegister.statusCode == 200) {
        SnackBar snackBarSuccess =
            const SnackBar(content: Text("Connexion réussie"));
        ScaffoldMessenger.of(context).showSnackBar(snackBarSuccess);
        Navigator.of(context).pushNamed('/home');

        tecPassword.clear();
        tecIdentifier.clear();

        _onLoginSuccess(jsonDecode(responseRegister.body)['jwt']);
      } else if (responseRegister.statusCode != null) {
        SnackBar snackBarSuccess = SnackBar(
            content:
                Text("Erreur : " + responseRegister.reasonPhrase.toString()));
        ScaffoldMessenger.of(context).showSnackBar(snackBarSuccess);
      }
    } on SocketException catch (socketException) {
      SnackBar snackBarSuccess = const SnackBar(
          content: Text("Nous avons des difficultés à joindre le serveur"));
      ScaffoldMessenger.of(context).showSnackBar(snackBarSuccess);
    }
  }

  _onRegister() async {
    Navigator.of(context).pushNamed('/register');
  }

  void _onLoginSuccess(String jwt) {
    var storage = FlutterSecureStorage();
    storage.write(key: "jwt", value: jwt);
    Navigator.pushNamed(context, "/home");
  }
}
